export interface GroupInfo {
  id: number;
  name: string;
  childGroups?: GroupInfo[];
}
