export interface User {
  id: number;
  username: string;
  groups: object;
}
