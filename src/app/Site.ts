import {GroupInfo} from './GroupInfo';

export interface Site {
  id: number;
  name: string;
  writeGroup: GroupInfo;
  readGroup?: GroupInfo;
}
