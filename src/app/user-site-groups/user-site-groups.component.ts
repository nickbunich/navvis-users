import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Site} from '../Site';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {GroupInfo} from '../GroupInfo';
import {FlatTreeControl} from '@angular/cdk/tree';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-user-site-groups',
  templateUrl: './user-site-groups.component.html',
  styleUrls: ['./user-site-groups.component.scss']
})
export class UserSiteGroupsComponent implements OnInit {
  @Input()
  public sites: Site[];

  @Output()
  public siteChange = new EventEmitter<number>();

  @Output()
  public groupChange = new EventEmitter<number>();

  @Output()
  public remove = new EventEmitter();
  public groupDataSource: MatTreeFlatDataSource<GroupInfo, object>;
  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  public nestedGroupNodeMap = new Map<GroupInfo, GroupFlatNode>();
  /** A selected parent node to be inserted */
  public selectedParentGroup: GroupFlatNode | null = null;
  public groupsTreeControl: FlatTreeControl<GroupFlatNode>;
  public groupsTreeFlattener: MatTreeFlattener<GroupInfo, GroupFlatNode>;
  public groupsDataSource: MatTreeFlatDataSource<GroupInfo, GroupFlatNode>;
  /** The selection for checklist */
  public checklistSelection = new SelectionModel<GroupInfo>(true /* multiple */);
  private Site: number;
  private Group: number;
  private sitesByIds = new Map<number, Site>();

  constructor() {
    this.groupsTreeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
      this.isExpandable, this.getChildren);
    this.groupsTreeControl = new FlatTreeControl<GroupFlatNode>(this.getLevel, this.isExpandable);
    this.groupsDataSource = new MatTreeFlatDataSource(this.groupsTreeControl, this.groupsTreeFlattener);
    this.groupsDataSource.data = [];
  }

  get site(): number {
    return this.Site;
  }

  @Input()
  set site(value: number) {
    this.Site = value;
    this.siteChange.emit(value);
    if (!value) {
      return;
    }
    const site = this.sitesByIds.get(value);
    this.checklistSelection.clear();
    this.groupsDataSource.data = [site.writeGroup];
  }

  get group(): number {
    return this.Group;
  }

  @Input()
  set group(value: number) {
    this.Group = value;
    this.groupChange.emit(value);
  }

  public getLevel = (node: GroupFlatNode) => node.level;

  public isExpandable = (node: GroupFlatNode) => node.item.childGroups?.length > 0;

  public getChildren = (node: GroupInfo): GroupInfo[] => node.childGroups ?? [];

  public hasChild = (_: number, NodeData: GroupFlatNode) => NodeData.item.childGroups?.length > 0;

  /**
   * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
   */
  public transformer = (node: GroupInfo, level: number) => {
    const existingNode = this.nestedGroupNodeMap.get(node);
    if (existingNode) {
      return existingNode;
    }
    const flatNode = {item: node, level};
    this.nestedGroupNodeMap.set(node, flatNode);
    return flatNode;
  }

  public toggleGroupSelection(node: GroupFlatNode): void {
    const wasSelected = this.checklistSelection.isSelected(node.item);
    this.checklistSelection.toggle(node.item);
    if (!wasSelected) {
      this.unselectChildGroups(node.item);
      this.unselectParentGroups(node);
    }
  }

  public unselectChildGroups(group: GroupInfo): void {
    const children = this.getChildren(group);
    this.checklistSelection.deselect(...children);
    children.forEach((child) => this.unselectChildGroups(child));
  }

  public unselectParentGroups(groupFlatNode: GroupFlatNode): void {
    const parent = this.getParentNode(groupFlatNode);
    if (!parent) {
      return;
    }
    this.checklistSelection.deselect(parent.item);
    this.unselectParentGroups(parent);
  }

  /* Get the parent node of a node */
  getParentNode(node: GroupFlatNode): GroupFlatNode {
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.groupsTreeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.groupsTreeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return undefined;
  }

  public doRemove(): void {
    this.remove.emit();
  }

  public ngOnInit(): void {
    this.sites.forEach((site) => this.sitesByIds.set(site.id, site));
  }

  public selectedGroupNames(): string {
    if (!this.checklistSelection.selected.length) {
      return '';
    }
    return this.checklistSelection.selected
      .map(s => s.name)
      .join(', ');
  }
}

/** Flat to-do item node with expandable and level information */
interface GroupFlatNode {
  item: GroupInfo;
  level: number;
}
