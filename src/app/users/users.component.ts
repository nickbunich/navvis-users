import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog} from '@angular/material/dialog';
import {AlertDialogComponent, AlertDialogData, AlertDialogResult} from '../AlertDialog/AlertDialogComponent';
import {Site} from '../Site';
import {User} from '../User';
import {UsersService} from '../users.service';
import {NewUserModalComponent} from '../new-user-modal-component/new-user-modal.component';
import {NewUserModalData} from '../new-user-modal-component/NewUserModalData';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  public sitesUsers = new Map<number, User[]>([
    [1, [
      {id: 1, username: 'ready', groups: {HQ: 'Admin'}},
      {id: 2, username: 'samson', groups: {HQ: 'Admin'}},
      {id: 3, username: 'lenny', groups: {HQ: 'Viewer'}},
      {id: 7, username: 'hans', groups: {HQ: 'Viewer', Basement: 'Publisher'}}
    ]],
    [2, [
      {id: 4, username: 'ken', groups: {Basement: 'Editor'}},
      {id: 5, username: 'bob', groups: {Basement: 'Viewer'}},
      {id: 6, username: 'carlo', groups: {Basement: 'Publisher'}},
      {id: 7, username: 'hans', groups: {HQ: 'Viewer', Basement: 'Publisher'}}
    ]]
  ]);

  public sitesByIds: Map<number, Site>;

  @ViewChild('deleteConfirmationMessage', {static: false})
  public deleteConfirmationMessage: TemplateRef<string>;

  public displayedColumns: string[] = ['select', 'user', 'group'];

  public selection = new SelectionModel<User>(true, []);

  public dataSource = new MatTableDataSource<User>();

  public allSitesValue = 0;

  public sites: Site[];

  private SelectedSite: number;

  constructor(private matDialog: MatDialog, private usersService: UsersService) {
  }

  public get selectedSite(): number {
    return this.SelectedSite;
  }

  public set selectedSite(value: number) {
    if (this.SelectedSite === value) {
      return;
    }
    this.selection.clear();
    if (this.SelectedSite === 0) {
      this.sites.forEach(() => this.displayedColumns.pop());
      this.displayedColumns.push('group');
    } else if (value === 0) {
      this.displayedColumns.pop();
      this.sites.forEach((site) => this.displayedColumns.push(site.name));
    }
    this.dataSource = new MatTableDataSource<User>(this.acquireUsers(value));
    this.SelectedSite = value;
  }

  ngOnInit(): void {
    this.setSites();
    this.selectedSite = this.allSitesValue;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  public isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  public masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  public checkboxLabel(row?: User): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} user ${row.username}`;
  }

  public showDeleteConfirmation() {
    this.matDialog.open<AlertDialogComponent, AlertDialogData, AlertDialogResult>(AlertDialogComponent, {
      data: {
        title: 'Delete user',
        body: this.deleteConfirmationMessage,
        declineButton: false,
        confirmButton: 'Delete',
        confirmButtonColor: 'warn',
      }
    }).afterClosed().subscribe((result) => {
      if (result === AlertDialogResult.CONFIRMED) {
        // tslint:disable-next-line:no-console
        console.info('Deleted');
      }
    });

  }

  public openNewUserModal() {
    this.matDialog.open<NewUserModalComponent, NewUserModalData, boolean>(NewUserModalComponent, {
      data: {sites: this.sites}
    }).afterClosed().subscribe((result) => {
      console.log('User created: ', result);
    });
  }

  private setSites(): void {
    this.sites = this.usersService.getSites();
    this.sitesByIds = new Map<number, Site>();
    this.sites.forEach((site) => this.sitesByIds.set(site.id, site));
  }

  private acquireUsers(siteId: number): User[] {
    if (siteId === 0) {
      const users = [];
      this.sitesUsers.forEach((siteUsers) => users.push(...siteUsers));
      return users;
    }
    return this.sitesUsers.get(siteId);
  }
}
