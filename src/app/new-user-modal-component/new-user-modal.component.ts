import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NewUserModalData} from './NewUserModalData';
import {Site} from '../Site';

@Component({
  selector: 'app-new-user-modal-component',
  templateUrl: './new-user-modal.component.html',
  styleUrls: ['./new-user-modal.component.scss']
})
export class NewUserModalComponent implements OnInit {

  public sites: Site[];

  public userGroupSelections: UserGroupSelection[] = [
    {site: undefined, group: undefined}
  ];

  constructor(public dialogRef: MatDialogRef<NewUserModalComponent>, @Inject(MAT_DIALOG_DATA) public data: NewUserModalData) {
    this.sites = data.sites;
  }

  ngOnInit(): void {
  }

  public cancel() {
    this.dialogRef.close(false);
  }

  public addUserGroupSelection() {
    this.userGroupSelections.push({site: undefined, group: undefined});
  }

  public removeGroup(i: number): void {
    this.userGroupSelections.splice(i, 1);
  }
}

interface UserGroupSelection {
  site: number;
  group: number;
}
