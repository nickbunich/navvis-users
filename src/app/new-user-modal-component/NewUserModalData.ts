import {Site} from '../Site';

export interface NewUserModalData {
  sites: Site[];
}
