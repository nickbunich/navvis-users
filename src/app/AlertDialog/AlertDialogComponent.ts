import {Component, HostListener, Inject, NgModule, TemplateRef} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {ThemePalette} from '@angular/material/core/common-behaviors/color';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';

/**
 * The possible results from the usage of the Alert Dialog modal
 */
export enum AlertDialogResult {
  /**
   * The confirm button has been pressed.
   */
  CONFIRMED,

  /**
   * The decline button has been pressed.
   */
  DECLINED,

  /**
   * The cancel button has been pressed.
   */
  CANCELLED
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'alert-dialog',
  templateUrl: './AlertDialog.ng.html',
  styleUrls: ['./AlertDialog.ng.scss'],
})
export class AlertDialogComponent {
  /**
   * The title of the modal.
   */
  public title: string;

  /**
   * The text content of the body.
   * If the provided {@link AlertDialogData.body} contains a string, this will be populated and
   * used in the template.
   */
  public bodyText: string;

  /**
   * The template content of the body.
   * If the provided {@link AlertDialogData.body} contains an html template, this will be
   * populated and injected in the modal's body template..
   */
  public bodyTemplate: TemplateRef<string>;

  /**
   * The text to be used in the cancel button.
   * See {@link AlertDialogData.cancelButton} to see its behaviour.
   */
  public cancelButtonText: string;

  /**
   * The text to be used in the cancel button.
   * See {@link AlertDialogData.declineButton} to see its behaviour.
   */
  public declineButtonText: string;

  /**
   * The text to be used in the cancel button.
   * See {@link AlertDialogData.confirmButton} to see its behaviour.
   */
  public confirmButtonText: string;

  /**
   * The color of the confirm button.
   */
  public confirmButtonColor: ThemePalette = 'primary';

  constructor(public dialogRef: MatDialogRef<AlertDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: AlertDialogData) {
    if (!this.data) {
      return;
    }

    this.title = data.title;
    this.confirmButtonColor = data.confirmButtonColor ?? 'primary';

    // Set the appropriate body variable
    if (data.body instanceof TemplateRef) {
      this.bodyTemplate = data.body;
    } else if (typeof data.body === 'string') {
      this.bodyText = data.body;
    }

    if (data.cancelButton !== false) {
      this.cancelButtonText = data.cancelButton ? `${data.cancelButton}` : 'Cancel';
    }

    if (data.declineButton !== false) {
      this.declineButtonText = data.declineButton ? `${data.declineButton}` : 'Decline';
    }

    if (data.confirmButton !== false) {
      this.confirmButtonText = data.confirmButton ? `${data.confirmButton}` : 'Confirm';
    }
  }

  @HostListener('window:beforeunload', ['$event'])
  public beforeUnload(event: Event): void {
    event.returnValue = true;
  }

  /**
   * Close the modal by signaling the confirm button has been clicked.
   */
  public confirm(): void {
    this.dialogRef.close(AlertDialogResult.CONFIRMED);
  }

  /**
   * Close the modal by signaling the decline button has been clicked.
   */
  public decline(): void {
    this.dialogRef.close(AlertDialogResult.DECLINED);
  }

  /**
   * Close the modal by signaling the cancel button has been clicked.
   */
  public cancel(): void {
    this.dialogRef.close(AlertDialogResult.CANCELLED);
  }
}

/**
 * The interface that dictates the type of the object passed to the {@link AlertDialogComponent}
 * used to define the text, aspect and colors of the modal.
 */
export interface AlertDialogData {
  /**
   * The title for the dialog. Usually the alert subject.
   */
  title: string;

  /**
   * The inner HTML of the body. You can use HTML tags here.
   */
  body: string | TemplateRef<string>;

  /**
   * The cancel button label.
   * If false, button will disappear.
   * If undefined, a default value will be used.
   */
  cancelButton?: string | false;

  /**
   * The decline button label.
   * If false, button will disappear.
   * If undefined, a default value will be used.
   */
  declineButton?: string | false;

  /**
   * The decline button label.
   * If false, button will disappear.
   * If undefined, a default value will be used.
   */
  confirmButton?: string | false;

  /**
   * Confirm button color
   */
  confirmButtonColor?: ThemePalette;
}

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule
  ],
  declarations: [
    AlertDialogComponent
  ],
  exports: [
    AlertDialogComponent
  ],
})
export class AlertDialogModule {

}
