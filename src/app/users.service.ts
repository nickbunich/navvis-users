import {Injectable} from '@angular/core';
import {Site} from './Site';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() {
  }

  public getSites(): Site[] {
    return [
      {
        id: 1,
        name: 'HQ',
        writeGroup: {
          id: 4,
          name: 'HQ:Admin',
          childGroups: [{id: 5, name: 'HQ:Editor', childGroups: [{id: 6, name: 'HQ:Viewer', childGroups: []}]}, {id: 99, name: 'HQ:Blah'}]
        }
      },
      {
        id: 2,
        name: 'Basement',
        writeGroup: {
          id: 7,
          name: 'Basement:Admin',
          childGroups: [{id: 8, name: 'Basement:Editor', childGroups: [{id: 9, name: 'Basement:Viewer', childGroups: []}]}]
        }
      },
      {
        id: 3,
        name: 'Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu',
        writeGroup: {
          id: 10,
          name: 'Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu:Admin',
          childGroups: [{
            id: 11,
            name: 'Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu:Editor',
            childGroups: [{
              id: 12,
              name: 'Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu:Viewer',
              childGroups: []
            }]
          }]
        }
      },
      {
        id: 4,
        name: 'Chargoggagoggmanchauggagoggchaubunagungamaugg',
        writeGroup: {
          id: 13,
          name: 'HQ:Admin',
          childGroups: [{id: 14, name: 'HQ:Editor', childGroups: [{id: 15, name: 'HQ:Viewer', childGroups: []}]}]
        }
      },
      {
        id: 5,
        name: 'Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch',
        writeGroup: {
          id: 16,
          name: 'HQ:Admin',
          childGroups: [{id: 17, name: 'HQ:Editor', childGroups: [{id: 18, name: 'HQ:Viewer', childGroups: []}]}]
        }
      },
      {
        id: 6,
        name: 'Tweebuffelsmeteenskootmorsdoodgeskietfontein',
        writeGroup: {
          id: 19,
          name: 'HQ:Admin',
          childGroups: [{id: 20, name: 'HQ:Editor', childGroups: [{id: 21, name: 'HQ:Viewer', childGroups: []}]}]
        }
      },
      {
        id: 7,
        name: 'Azpilicuetagaraycosaroyarenberecolarrea',
        writeGroup: {
          id: 22,
          name: 'HQ:Admin',
          childGroups: [{id: 23, name: 'HQ:Editor', childGroups: [{id: 24, name: 'HQ:Viewer', childGroups: []}]}]
        }
      },
      {
        id: 8,
        name: 'Äteritsiputeritsipuolilautatsijänkä',
        writeGroup: {
          id: 25,
          name: 'HQ:Admin',
          childGroups: [{id: 26, name: 'HQ:Editor', childGroups: [{id: 27, name: 'HQ:Viewer', childGroups: []}]}]
        }
      },
      {
        id: 9,
        name: 'Pekwachnamaykoskwaskwaypinwanik',
        writeGroup: {
          id: 28,
          name: 'HQ:Admin',
          childGroups: [{id: 29, name: 'HQ:Editor', childGroups: [{id: 30, name: 'HQ:Viewer', childGroups: []}]}]
        }
      },
      {
        id: 10,
        name: 'Venkatanarasimharajuvaripeta',
        writeGroup: {
          id: 31,
          name: 'HQ:Admin',
          childGroups: [{id: 32, name: 'HQ:Editor', childGroups: [{id: 33, name: 'HQ:Viewer', childGroups: []}]}]
        }
      }
    ];
  }
}
